
/* ---------- Déclaration de toutes les variables ----------------------- */
const Discord = require('discord.js');
const {
  prefix,
  token,
} = require('./config.json');
const client = new Discord.Client();
const fs = require('fs');
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
client.commands = new Discord.Collection();
const cooldowns = new Discord.Collection();
const now = Date.now();
const timestamps = cooldowns.get(command.name);
const cooldownAmount = (command.cooldown || 3) * 1000;


/* Permets la création de la constante commandName en fonction d'une commande existante dans le dossier Commands */
for (const file of commandFiles) {
  const commandName = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

/*-------------- Commandes et fonctions liées aux changements de Status du bot ----------- */
/* Commande permettant au bot de se connecter*/
client.login(token); 

client.once('ready', () => {
  client.user.setActivity("Apprentissage des beurres LU") //Losrque le bot est ready, on définit son activité
  console.log('Ready!');                    // + un message est indiqué dans la console
});

client.once('reconnecting', () => { // Losrque le bot se reconnecte, un message est indiqué dans la console
  console.log('Reconnecting!');
});

client.once('disconnect', () => {
  console.log('Disconnect!'); // Losrque le bot est déconnecté, un message est indiqué dans la console
});

/*-------------- Commandes et fonctions liées aux intéractions avec le bot -------------------------*/

client.on("message", function (message) {
  if (message.author.bot) return; // Si le message sur discord provient d'un bot alors on arrête le traitement
  if (!message.content.startsWith(prefix)) return; // Si le message ne commence pas avec le prefix "!" alros on arrête le traitement
  const commandBody = message.content.slice(prefix.length);
  const args = message.content.slice(prefix.length).trim().split(/ +/);
  const command = args.shift().toLowerCase();

  /* Créer une variable intitulé "command" qui corresponds à l'actuel command object*/
  	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;

  /* Vérifie et empêche d'utiliser les commandes associées dans les DM*/
  if (command.guildOnly && message.channel.type === 'dm') {
    return message.reply('Vous ne pouvez pas exécuter cette commande dans les messages privés!');
  }

  /* Certaines commandes auront une constante arg= true et permettrons d'avoir ce message*/
  if (command.args && !args.length) {
    let reply = `Vous n'avez donné aucun argument, ${message.author}!`;

    if (command.usage) {
      reply += `\nLa bonne façon d'utiliser la commande est : \`${prefix}${command.name} ${command.usage}\``;
    }
    return message.channel.send(reply);
  }

/* Commandes liées aux cooldowns/temps d'attentes entre deux messages/appels bots */
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }
  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

	if (now < expirationTime) {
		const timeLeft = (expirationTime - now) / 1000;
		return message.reply(`Attendez s'il vous plaît ${timeLeft.toFixed(1)}  seconde(s) avant de réutiliser la commande\`${command.name}\`.`);
	}
  }
  //Permets la suppression automatique du message après le délai imparti s'il n'y a pas eu de réponse de l'user
  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  try {
    // Commandes dynamiques : récupère l'intitulé de la commande se trouvant dans le dossier Commands et qui corresponds au message tapé par l'utilisateur
    command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply('Il y a eut une erreur en executant la commande !'); // Permets de prendre le cas d'une erreur et de retourner un message
  }


});




