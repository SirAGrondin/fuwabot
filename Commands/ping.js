module.exports = {
	name: 'ping',
	description: 'Send you a Pong + tells your ping!',
	cooldown: 5,
		execute(message, args) {
		const timeTaken = Date.now() - message.createdTimestamp;
		message.reply(`Pong! This message had a latency of ${timeTaken}ms.`);
	},
};