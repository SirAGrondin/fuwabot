module.exports = {
	name: 'kick',
	description: 'Tag a member and kick them (but not really).',
	guildOnly: true,
	aliases :['kickout'],

	execute(message) {
    //Since message.mentions.users is a Collection, it has a .size property. 
    //If no users are mentioned, it'll return 0 (which is a falsy value), meaning you can do if (!value) to check if it's falsy.
		if (!message.mentions.users.size) { 
			return message.reply('you need to tag a user in order to kick them!');
		}
		const taggedUser = message.mentions.users.first();
		message.channel.send(`You wanted to kick: ${taggedUser.username}`);
	},
};